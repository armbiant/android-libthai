Just run "make" to create the update+sorted version of tdict.txt from
tdict.org and tdict.local.

Files description
=================
tdict.org	-- Summary of words from nectec/
nectec/		-- Words from NECTEC archive
tdict.local 	-- Add your local words here (former tdict.hui from original cttex)

dictsort.c 	-- This will read tdict.nonsorted, sort it and write to tdict.sorted
tdict.nonsorted -- generated by "make" from the mix of tdict.org+tdict.local
tdict.txt	-- output from dictsort (run via "make")
